#!/usr/bin/env python3

# File: overlap.py
# Auth: Randall Morgan
# Desc: This file demonstrates
# how to find the area of overlap
# between two rectangles.

class rect:
    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

def dist(r1x1, r2x1, r1x2, r2x2):
    return min(r1x2, r2x2) - max(r1x1, r2x1)


def overlap_area(r1, r2):
    xdist = dist(r1.x1, r2.x1, r1.x2, r2.x2)
    ydist = dist(r1.y1, r2.y1, r1.y2, r2.y2)

    if xdist <= 0 or ydist <= 0:
        return 0
    else:
        return xdist * ydist

if __name__ == "__main__":
    r1 = rect(1, 3, 6, 6)
    r2 = rect(3, 5, 8, 8)
    print(overlap_area(r1, r2))