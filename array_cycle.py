#
# Given an array where each element of the array
# coresponds to an index within the same array,
# how can you determine if their is a cycle or not?
#
# _______________
# |1|2|1|2|3|4|8|
#              ^
#              |
# This digit is the end digit and must be greater 
# than the array length to indicate it's end.
#
# We use Floyd's or Brent's algorithm to solve this
# Also known as the tortoise and hare algorithm.

def cycle(arr):
    p = 0
    q = 0
    count = 0
    while True:
        print("Iteration: " + str(count))
        print("p: " + str(p) + " q: " + str(q))

        if p < 0 or q < 0 or p >= len(arr) or q >= len(arr):
            print("exit 1")
            return False
        
        p = arr[p]
        print("p: " + str(p) + " q: " + str(q))
        if p < 0 or p >= len(arr):
            print("exit 2")
            return False
        if p == q:
            print("exit 3")
            return True
        
        p = arr[p]
        print("p: " + str(p) + " q: " + str(q))
        if p == q:
            print("exit 4")
            return True
        
        q = arr[q]
        print("p: " + str(p) + " q: " + str(q))
        if p == q:
            print("exit 5")
            return True
        
        count += 1

arr = [1,2,3,4,1,1,8]

print(cycle(arr))

        