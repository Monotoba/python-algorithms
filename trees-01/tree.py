#!/usr/bin/env python

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


# Tree:    A
#         / \
#        B   C
#       / \
#      D   E
#

# Create nodes
root = Node('A')
n1 = Node('B')
n2 = Node('C')
n3 = Node('D')
n4 = Node('E')

# Setup children
root.left = n1
root.right = n2
n1.left = n3
n1.right = n4


# Preorder traversal
# 1) Return the root node value.
# 2) Traverse the left subtree by recursively calling the pre-order function.
# 3) Traverse the right subtree by recursively calling the pre-order function.
#
def pre_order(root, nodes):
    nodes.append(root.data)
    if root and root.left:
        pre_order(root.left, nodes)
 
    if root and root.right:
        pre_order(root.right, nodes)
    return nodes


# In Order traversal
# 1) Traverse the left subtree by recursively calling the in-order function.
# 2) Return the root node value.
#  3) Traverse the right subtree by recursively calling the in-order function.
#
def in_order(root, nodes):
    if root and root.left:
        in_order(root.left, nodes)
    nodes.append(root.data)
    if root and root.right:
        in_order(root.right, nodes)
    return nodes


# Post Order traversal
# 1) Traverse the left subtree by recursively calling the post-order function. 
# 2) Traverse the right subtree by recursively calling the post-order function.
# 3) Return the root node value.
#
def post_order(root, nodes):
    if root and root.left:
        post_order(root.left, nodes)
    if root and root.right:
        post_order(root.right, nodes)
    nodes.append(root.data)
    return nodes


# Breath First Order Traversal
# 1) Add the root to a queue. 
# 2) Pop the last node from the queue, and return its value. 
# 3) Add all children of popped node to queue, and continue 
# from step 2 until queue is empty.
#
def breath_first_order(root, nodes):
    queue = [root]
    while queue:
        n = queue.pop(0)
        nodes.append(n.data)
        if n.left:
            queue.append(n.left)
        if n.right:
            queue.append(n.right)
    return nodes


print pre_order(root, []) # => ['A', 'B', 'D', 'E', 'C' ]

print in_order(root, []) # => ['D', 'B', 'E', 'A', 'C']

print post_order(root, []) # => ['D', 'E', 'B', 'C', 'A']

print breath_first_order(root, []) # => ['A', 'B', 'C', 'D', 'E']