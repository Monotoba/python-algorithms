#!/usr/bin/env python

class Node:
    # Constructor
    def __init__(self, label=None, data=None):
        self.label = label
        self.data = data
        self.children = dict()

    def addChild(self, key, data=None):
        if not isinstance(key, Node):
            self.children[key] = Node(key, data)
        else:
            self.children[key.label] = key

    def __getitem__(self, key):
        return self.children[key]


    
class Trie:
    def __init__(self):
        self.head = Node()

    def __getitem__(self, key):
        return self.head.children[key]

    def add(self, word):
        current_node = self.head
        word_finished = True

        # search tree for word or partial word
        for i in range(len(word)):
            if word[i] in current_node.children:
                current_node = current_node.children[word[i]]
            else:
                word_finished = False
                break

        # For every new letter, create a new child node
        if not word_finished:
            while i < len(word):
                current_node.addChild(word[i])
                current_node = current_node.children[word[i]]
                i += 1

        # Store the full word at the end node so we 
        # don't need to traverse back up the tree to 
        # reconstruct it.
        current_node.data = word

    def has_word(self, word):
        if word == '':
            return False
        if word == Node:
            raise ValueError('Trie.has_word requires a non-Null string')

        # Start at the top
        current_node = self.head
        exists = True
        for letter in word:
            if letter in current_node.children:
                current_node = current_node.children[letter]
            else:
                exists = False
                break

        # Still need to check if we just reached a word like "t"
        # that isn't actually a full word in our dictionary
        if exists:
            if current_node.data == None:
                exists = False

        
        return exists

    def starts_with_prefix(self, prefix):
        # Retuns a list of all words in the tree that start with prefix
        words = list()
        if prefix == Node:
            raise ValueError('Requires non-Null prefix')

        top_node = self.head
        for letter in prefix:
            if letter in top_node.children:
                top_node = top_node.children[letter]
            else:
                # Prefix not in tree, so stop
                return words

        # Get words under prefix
        if top_node == self.head:
            queue = [node for key, node in top_node.children.iteritems()]
        else: 
            queue = [top_node]

        # Perform a breath first search under the prefix
        # A cool effect of using BFS as opposed to DFS is that BFS will return
        # a list of words ordered by increasing length
        while queue:
            current_node = queue.pop()
            if current_node.data != None:
                # Isn't it nice to not have to go back up the tree?
                words.append(current_node.data)

            queue = [node for key, node in current_node.children.iteritems()] + queue

        return words


    def getData(self, word):
        # This returns the 'data' of the node identified by the given word
        if not self.has_word(word):
            raise ValueError('{} not found in trie'.format(word))

        # Race to the bottom, get data
        current_node = self.head
        for letter in word:
            current_node = current_node[letter]

        return current_node.data


# Run the program
if __name__ == '__main__':
    # Example use
    trie = Trie()
    words = 'hello world foo bar fuzz buzz help gold howard tea ted team to too two tom stan standard stanford education money'
    for word in words.split():
        trie.add(word)

    print "'goodbye' in trie: ", trie.has_word('goodbye')
    print trie.starts_with_prefix('g')
    print trie.starts_with_prefix('to')



