
from Tkinter import *
import time

# Graph offset
offsetX = 100
offsetY = 250

# Data to plot/sort
data = [9,1,5,3,8,5,6,2,3,8,4,7]

swapped = False


def drawBar(canvas, bars):
    for bar in bars:
        canvas.create_rectangle(bar, fill="green")

    


def main():
    root = Tk()
    root.title('Simple Plot - Version 3 - Smoothed')

    try:
        canvas = Canvas(root, width=450, height=300, bg = 'white')
        canvas.pack()
        Button(root, text='Quit', command=root.quit).pack()
    except:
        print 'An error has occured!'

    root.mainloop()

    bubbleSort(canvas, data)

    #root.mainloop()
    
    

def drawAxis(canvas):
   
    canvas.create_line(100,250,400,250, width=2)
    canvas.create_line(100,250,100,50,  width=2)

    # Draw x axis
    for i in range(11):
        x = 100 + (i * 30)
        canvas.create_line(x,250,x,245, width=2)
        canvas.create_text(x,254, text='%d'% (10*i), anchor=N)

    # Draw y axis
    for i in range(11):
        y = 250 - (i * 20)
        canvas.create_line(100, y,105,y, width=2)
        canvas.create_text(96,y, text='%d'% (10*i), anchor=E)


def bubbleSort(canvas, data):
    for i in range(len(data)-1, 0 ,-1):
        for j in range(i):
            canvas.delete("all")
            drawAxis(canvas)
            bars = calcBars(data)
            drawBar(canvas, bars)

            if data[j] > data[j+1]:
                tmp = data[j]
                data[j] = data[j+1]
                data[j+1] = tmp
            time.sleep(1)
                
    #return data


# Calculates the bar position for each datum
def calcBars(data):
    bars = []
    for i in range(len(data)):
        x1 = 100+(i*5)
        y1 = 250-2
        x2 = x1+3
        y2 = 250-(data[i]*10)
        bars.append([x1, y1, x2, y2])

    return bars;

main()